import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './interceptors/axios'
// @ts-ignore
import gAuth from 'vue3-google-auth'

const GAuth = gAuth.createGAuth({
  clientId:
    '576612116326-c0b1tujc0ejijo0p5ntcjhckhjoklus6.apps.googleusercontent.com',
})

createApp(App).use(store).use(router).use(GAuth).mount('#app')
